#
# This file is part of mavmit <https://gitlab.com/rm/mavmit>.
# mavmit allows to transmit files via mavlink connections and to measure connection quality
#
# Copyright (C) 2021 Richard Meinsen
#
# mavmit is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# navmit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#


import time

from watchdog.observers import Observer

from mavmit import appconfig as CNF
from mavmit import applogging
from mavmit import datafilewrapper
from mavmit import filehandler
from mavmit import mavlinkhelper
from mavmit import mavmitfieldnames as MFN
from mavmit.transmissiondecoder import TransmissionDecoder

log = applogging.get_applogger()


class Dispatcher:

    def __init__(self, args, sendqueues, mavlink_connection):

        self.__args = args
        self.__srcPath = args.inputdir

        self.__sendqueues = sendqueues

        self.__mavlink_connection = mavlink_connection

        self.__filehandler = filehandler.FileHandler(sendqueues)

        self.__transmissiondecoder = TransmissionDecoder(args)

        self.__observer = Observer()

        self.__dispatch_start = time.time()
        self.__dispatch_end = None
        if args.duration > 0:
            self.__dispatch_end = self.__dispatch_start + args.duration

        self.__dispatch_timestamp = 0.0
        self.__dispatch_timestamp_prev = 0.0

        self.__heartbeat_timestamp = 0.0
        # self.__heartbeat_timestamp_prev = 0.0

        self.__datapacket_timestamp = 0.0
        self.__imagepacket_timestamp = 0.0

        self.__packets_file = None
        self.__sentpackets_file = None
        self.__receivedpackets_file = None

    def run(self):
        self.start()
        try:
            while True:
                time.sleep(CNF.DISPATCHER_UPDATE_INTERVAL_S)
                if not self.dispatch():
                    break
        except KeyboardInterrupt:
            log.info("KeyboardInterrupt: abort by user")
        self.stop()

    def start(self):
        header_dict = datafilewrapper.args_to_header_dict(self.__args)

        if self.__args.packetsfile is not None:
            self.__packets_file \
                = datafilewrapper.DataFileWrapper(self.__args.packetsfile, header_dict)

        if self.__args.sentpacketsfile is not None:
            self.__sentpackets_file \
                = datafilewrapper.DataFileWrapper(self.__args.sentpacketsfile, header_dict)

        if self.__args.receivedpacketsfile is not None:
            self.__receivedpackets_file \
                = datafilewrapper.DataFileWrapper(self.__args.receivedpacketsfile, header_dict)

        self.__schedule()
        self.__observer.start()

    def stop(self):
        self.__observer.stop()
        self.__observer.join()

        if self.__args.packetsfile is not None:
            self.__packets_file.finish()

        if self.__args.sentpacketsfile is not None:
            self.__sentpackets_file.finish()

        if self.__args.receivedpacketsfile is not None:
            self.__receivedpackets_file.finish()

    def __schedule(self):
        self.__observer.schedule(
            self.__filehandler,
            self.__srcPath,
            recursive=False
        )

    def write_received_msg_dict(self, msg_dict):
        tmp_msgbuf = None
        tmp_data = None

        if MFN.MSGDICT_MSG_BUF in msg_dict.keys():
            tmp_msgbuf = msg_dict[MFN.MSGDICT_MSG_BUF]
            del msg_dict[MFN.MSGDICT_MSG_BUF]

        if MFN.MSGDICT_DATA in msg_dict.keys():
            tmp_data = msg_dict[MFN.MSGDICT_DATA]
            del msg_dict[MFN.MSGDICT_DATA]

        if self.__receivedpackets_file:
            self.__receivedpackets_file.write_item(msg_dict)

        if self.__packets_file:
            self.__packets_file.write_item(msg_dict)

        if tmp_msgbuf:
            msg_dict[MFN.MSGDICT_MSG_BUF] = tmp_msgbuf

        if tmp_data:
            msg_dict[MFN.MSGDICT_DATA] = tmp_data

    def write_sent_msg_dict(self, msg_dict):
        tmp_data = None
        if MFN.MSGDICT_DATA in msg_dict.keys():
            tmp_data = msg_dict[MFN.MSGDICT_DATA]
            del msg_dict[MFN.MSGDICT_DATA]

        if self.__sentpackets_file:
            self.__sentpackets_file.write_item(msg_dict)
        if self.__packets_file:
            self.__packets_file.write_item(msg_dict)

        if tmp_data:
            msg_dict[MFN.MSGDICT_DATA] = tmp_data

    def dispatch(self):

        self.__dispatch_timestamp_prev = self.__dispatch_timestamp
        self.__dispatch_timestamp = time.time()

        if self.__dispatch_end and self.__dispatch_end < self.__dispatch_timestamp:
            log.info("end of execute duration reached -> terminating")
            return False

        # dispatch_deltatime could be used for dispatch rate limiting
        dispatch_deltatime = self.__dispatch_timestamp - self.__dispatch_timestamp_prev
        heartbeat_deltatime = self.__dispatch_timestamp - self.__heartbeat_timestamp

        recvd_msg_dicts = mavlinkhelper.attempt_recv_msgdicts_wo_timeout(self.__mavlink_connection)
        if recvd_msg_dicts is not None:

            for recvd_msg_dict in recvd_msg_dicts:
                log.debug("a: got message %s" % recvd_msg_dict[MFN.MSGDICT_MSG_TYPE])
                self.write_received_msg_dict(recvd_msg_dict)

                self.__transmissiondecoder.transition(recvd_msg_dict)

                # get radio status
                #   eval radio status for send pressure
                # get heatbeat status
                #    eval heatbeat status -> maybe stop sending when heartbeat missing
                # get other packets
                #   to be decided

        if CNF.DISPATCHER_SEND_HEARTBEAT and heartbeat_deltatime >= CNF.DISPATCHER_MAVLINK_HEARTBEAT_INTERVAL_S:
            log.debug("b: send heartbeat")
            self.__heartbeat_timestamp = self.__dispatch_timestamp
            sent_msg_dict = mavlinkhelper.send_heartbeat(
                self.__mavlink_connection,
                self.__args)
            self.write_sent_msg_dict(sent_msg_dict)

        elif self.__sendqueues.has_packet():
            log.debug("c: send packet")

            msg_dict_packet = self.__sendqueues.get_packet()

            sent_msg_dict, result = mavlinkhelper.send_packet_from_msg_dict(
                self.__mavlink_connection,
                self.__args,
                msg_dict_packet)
            self.write_sent_msg_dict(sent_msg_dict)

        elif self.__sendqueues.has_datafile_name() or self.__sendqueues.has_imagefile_name():
            log.debug("d: prepare file to send")

            # short datafiles get higher prio
            if self.__sendqueues.has_datafile_name():
                datafile_name = self.__sendqueues.get_datafile_name()
                self.__filehandler.process_file(datafile_name)

            elif self.__sendqueues.has_imagefile_name():
                imagefile_name = self.__sendqueues.get_imagefile_name()
                self.__filehandler.process_file(imagefile_name)

        elif self.__sendqueues.has_packet():
            log.debug("e: send packet after file prep")

            msg_dict_packet = self.__sendqueues.get_packet()
            sent_msg_dict, result = mavlinkhelper.send_packet_from_msg_dict(
                self.__mavlink_connection,
                self.__args,
                msg_dict_packet)
            self.write_sent_msg_dict(sent_msg_dict)

        else:
            log.debug("f: nothing do do")

        return True
