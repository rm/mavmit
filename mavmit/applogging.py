#
# This file is part of mavmit <https://gitlab.com/rm/mavmit>.
# mavmit allows to transmit files via mavlink connections and to measure connection quality
#
# Copyright (C) 2021 Richard Meinsen
#
# mavmit is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# navmit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#


import logging

from mavmit import appconfig


def configure_applogging(logfile_name):
    log_name = appconfig.APP_NAME
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s %(name)-4s %(levelname)-7s %(message)s',
        filemode='w')

    logger = logging.getLogger(log_name)
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.FileHandler(logfile_name))
    return logger


def get_applogger():
    return logging.getLogger(appconfig.APP_NAME)
