#
# This file is part of mavmit <https://gitlab.com/rm/mavmit>.
# mavmit allows to transmit files via mavlink connections and to measure connection quality
#
# Copyright (C) 2021 Richard Meinsen
#
# mavmit is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# navmit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#


APP_NAME = "mavmit"

APP_LICENSE = \
    """
    
    mavmit allows to transmit files via mavlink connections and to measure connection quality
    
    Copyright (C) 2021 Richard Meinsen
    
    mavmit is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.
    
    navmit is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
    
    """

DISPATCHER_MAVLINK_HEARTBEAT_INTERVAL_S = 1.0
DISPATCHER_UPDATE_INTERVAL_S = 0.1
DISPATCHER_SEND_HEARTBEAT = True

MAVLINKHELPER_RECV_TIMEOUT_HEARTBEAT_S = 0.2
MAVLINKHELPER_RECV_WAIT_HEARTBEAT_S = 0.5
MAVLINKHELPER_RECV_WAIT_ANY_S = 0.5
