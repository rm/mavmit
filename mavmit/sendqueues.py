#
# This file is part of mavmit <https://gitlab.com/rm/mavmit>.
# mavmit allows to transmit files via mavlink connections and to measure connection quality
#
# Copyright (C) 2021 Richard Meinsen
#
# mavmit is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# navmit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#


from collections import deque


class SendQueues:

    def __init__(self, imagefiles, datafiles, packets):
        self.__image_filename_queue = deque(imagefiles)
        self.__data_filename_queue = deque(datafiles)
        self.__packet_queue = deque(packets)

    def get_imagefile_name(self):
        if len(self.__image_filename_queue) > 0:
            return self.__image_filename_queue.popleft()
        else:
            return None

    def has_imagefile_name(self):
        if len(self.__image_filename_queue) > 0:
            return True
        else:
            return False

    def get_datafile_name(self):
        if len(self.__data_filename_queue) > 0:
            return self.__data_filename_queue.popleft()
        else:
            return None

    def has_datafile_name(self):
        if len(self.__data_filename_queue) > 0:
            return True
        else:
            return False

    def put_imagefile_name(self, filename):
        self.__image_filename_queue.append(filename)

    def put_datafile_name(self, filename):
        self.__data_filename_queue.append(filename)

    def has_packet(self):
        if len(self.__packet_queue) > 0:
            return True
        else:
            return False

    def get_packet(self):
        if len(self.__packet_queue) > 0:
            return self.__packet_queue.popleft()
        else:
            return None

    def put_packets(self, packets):
        self.__packet_queue.extend(packets)

    def put_packet(self, packet):
        self.__packet_queue.append(packet)
