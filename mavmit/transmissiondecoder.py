#
# This file is part of mavmit <https://gitlab.com/rm/mavmit>.
# mavmit allows to transmit files via mavlink connections and to measure connection quality
#
# Copyright (C) 2021 Richard Meinsen
#
# mavmit is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# navmit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#


from enum import Enum
from pathlib import Path

from mavmit import applogging
from mavmit import filehandler
from mavmit import mavlinkhelper
from mavmit import mavmitfieldnames as MFN

log = applogging.get_applogger()


class State(Enum):
    NEUTRAL = 0
    DATA_TRANSMISSION_HANDSHAKE = 130
    ERROR = 256


class TransmissionDecoder:

    def __init__(self, args):

        self.__args = args
        self.__state = State.NEUTRAL

        self.__data_transmission_handshake_dict = None
        self.__encapsulated_data_dicts = None

    def process_and_write_received_statustext(self, msg_dict):

        msg_recvd_timestamp = int(msg_dict[MFN.MSGDICT_RECV_TIMESTAMP_US])

        text = msg_dict[MFN.MSGDICT_STATUSTEXT_TEXT]
        severity = msg_dict[MFN.MSGDICT_STATUSTEXT_SEVERITY]
        content = "{}:{}".format(text, str(severity))
        file_ext = filehandler.STATUSTEXT_FILEEXTENSION

        filename_with_ext = "{}.{}".format(str(msg_recvd_timestamp), file_ext)
        statustext_path_w_filename = Path(self.__args.outputdir) / filename_with_ext

        with open(statustext_path_w_filename, "wt") as statustext_file:
            statustext_file.write(content)
            log.info("statustext written to %s", statustext_path_w_filename)

    def process_and_write_received_named_value(self, msg_dict):

        msg_type = msg_dict[MFN.MSGDICT_MSG_TYPE]
        msg_time_since_boot_ms = msg_dict[MFN.MSGDICT_TIME_SINCE_BOOT_MS]
        msg_recvd_timestamp = int(msg_dict[MFN.MSGDICT_RECV_TIMESTAMP_US])

        if msg_type == mavlinkhelper.MSG_TYPE_NAMED_VALUE_FLOAT:
            name = msg_dict[MFN.MSGDICT_NAMED_VALUE_NAME]
            value = msg_dict[MFN.MSGDICT_NAMED_VALUE_FLOAT_VALUE]
            content = "{}:{}".format(name, str(value))
            file_ext = filehandler.NAMEDVALUE_FILEEXTENSION_FLOAT

        elif msg_type == mavlinkhelper.MSG_TYPE_NAMED_VALUE_INT:
            name = msg_dict[MFN.MSGDICT_NAMED_VALUE_NAME]
            value = msg_dict[MFN.MSGDICT_NAMED_VALUE_INT_VALUE]
            content = "{}:{}".format(name, str(value))
            file_ext = filehandler.NAMEDVALUE_FILEEXTENSION_INT

        else:
            log.warn("failed to assemble named value")
            return

        filename_with_ext = "{}.{}".format(str(msg_recvd_timestamp), file_ext)
        namedvalue_path_w_filename = Path(self.__args.outputdir) / filename_with_ext

        with open(namedvalue_path_w_filename, "wt") as imagefile:
            imagefile.write(content)
            log.info("named value written to %s", namedvalue_path_w_filename)

    def save_content_data(self, content_data):
        data_transmission_handshake_recvd_timestamp = \
            int(self.__data_transmission_handshake_dict[MFN.MSGDICT_RECV_TIMESTAMP_US])
        file_ext = mavlinkhelper.ext_from_stream_type(
            self.__data_transmission_handshake_dict[
                MFN.MSGDICT_MAVLINK_DATA_STREAM_TYPE]
        )

        filename_with_ext = "{}.{}".format(str(data_transmission_handshake_recvd_timestamp), file_ext)
        content_path_w_filename = Path(self.__args.outputdir) / filename_with_ext

        with open(content_path_w_filename, "wb") as content_file:
            content_file.write(content_data)
            log.info("content written to %s", content_path_w_filename)

    def assemble_content_data(self):
        bytes_num = self.__data_transmission_handshake_dict[
            MFN.MSGDICT_BYTES_NUM]
        packets_num = self.__data_transmission_handshake_dict[
            MFN.MSGDICT_PACKETS_NUM]

        content_data = bytearray()
        for idx, encapsulated_data_dict in enumerate(self.__encapsulated_data_dicts):
            packet_data = encapsulated_data_dict[MFN.MSGDICT_DATA]
            packet_data_len = len(packet_data)
            content_data_len = len(content_data)
            log.debug("len(content_data) %d , len(packet_data) %d",
                      content_data_len, packet_data_len)
            if idx == packets_num - 1:
                relevant_data_len = bytes_num - content_data_len
                log.debug("append relevant_data_len %d ", relevant_data_len)
                for index in range(0, relevant_data_len):
                    content_data.append(packet_data[index])
                log.debug("appended content data len %d ", len(content_data))
            else:
                data = encapsulated_data_dict[MFN.MSGDICT_DATA]
                log.debug("extend data len %d ", len(data))
                content_data.extend(data)
                log.debug("extended content data len %d ", len(content_data))
        return content_data

    def process_and_write_received_encapsulated_data(self):
        new_state = None
        received_data = None
        stream_type = self.__data_transmission_handshake_dict[
            MFN.MSGDICT_MAVLINK_DATA_STREAM_TYPE]
        image_width = self.__data_transmission_handshake_dict[
            MFN.MSGDICT_IMAGE_WIDTH]
        image_height = self.__data_transmission_handshake_dict[
            MFN.MSGDICT_IMAGE_HEIGHT]
        image_quality_jpg = self.__data_transmission_handshake_dict[
            MFN.MSGDICT_IMAGE_JPG_QUALITY]
        bytes_num = self.__data_transmission_handshake_dict[
            MFN.MSGDICT_BYTES_NUM]
        packets_num = self.__data_transmission_handshake_dict[
            MFN.MSGDICT_PACKETS_NUM]
        packet_payload_bytes = self.__data_transmission_handshake_dict[
            MFN.MSGDICT_PACKET_PAYLOAD_BYTES]

        encapsulated_data_dicts_count = len(self.__encapsulated_data_dicts)
        if encapsulated_data_dicts_count == packets_num:
            log.debug(
                "received expected number ENCAPSULATED_DATA packets.\n"
                "further checks could/should be done.\n"
                "for now just assembling the received data.\n"
                "a missing packet for now means missing the whole file.\n"
                "this could be improved depending on picture type or \n"
                "raw data type and error rate.\n")
            received_data = self.assemble_content_data()
            if received_data is not None:
                log.debug("successfully assembled received data")
                self.save_content_data(received_data)
            else:
                log.warn("failed to assemble received data")

            return State.NEUTRAL, received_data
        else:
            log.debug("waiting to receive the expected number ENCAPSULATED_DATA packets")
            return State.DATA_TRANSMISSION_HANDSHAKE, None

    def transition(self, msg_dict):
        msg_type = msg_dict[MFN.MSGDICT_MSG_TYPE]

        if self.__state == State.NEUTRAL:

            if msg_type == mavlinkhelper.MSG_TYPE_DATA_TRANSMISSION_HANDSHAKE:
                log.info("State.NEUTRAL: received DATA_TRANSMISSION_HANDSHAKE")
                # https://mavlink.io/en/messages/common.html#DATA_TRANSMISSION_HANDSHAKE
                # DATA_TRANSMISSION_HANDSHAKE ( #130 )

                self.__state = State.DATA_TRANSMISSION_HANDSHAKE
                self.__data_transmission_handshake_dict = msg_dict
                self.__encapsulated_data_dicts = []

                log.debug("MAVLINK_DATA_STREAM_TYPE %d",
                          msg_dict[MFN.MSGDICT_MAVLINK_DATA_STREAM_TYPE])
                # uint8_t	MAVLINK_DATA_STREAM_TYPE    Type of requested/acknowledged data.
                log.debug("MSGDICT_IMAGE_WIDTH %d",
                          msg_dict[MFN.MSGDICT_IMAGE_WIDTH])
                # width	    uint16_t	    Width of a matrix or image.
                log.debug("MSGDICT_IMAGE_HEIGHT %d",
                          msg_dict[MFN.MSGDICT_IMAGE_HEIGHT])
                # height	uint16_t	    Height of a matrix or image.
                log.debug("MSGDICT_IMAGE_QUALITY_JPG %d",
                          msg_dict[MFN.MSGDICT_IMAGE_JPG_QUALITY])
                # size	    uint32_t	    bytes		total data size (set on ACK only).
                log.debug("MSGDICT_BYTES_NUM %d",
                          msg_dict[MFN.MSGDICT_BYTES_NUM])
                log.debug("MSGDICT_PACKETS_NUM %d",
                          msg_dict[MFN.MSGDICT_PACKETS_NUM])
                # packets	uint16_t	    Number of packets being sent (set on ACK only).
                log.debug("MSGDICT_PACKET_PAYLOAD_BYTES %d",
                          msg_dict[MFN.MSGDICT_PACKET_PAYLOAD_BYTES])
                # payload	uint8_t	bytes	Payload size per packet

            elif msg_type == mavlinkhelper.MSG_TYPE_ENCAPSULATED_DATA:
                log.warn("State.NEUTRAL: received ENCAPSULATED_DATA -> unexpected")

            elif msg_type == mavlinkhelper.MSG_TYPE_NAMED_VALUE_INT or \
                    msg_type == mavlinkhelper.MSG_TYPE_NAMED_VALUE_FLOAT:
                log.info("State.NEUTRAL: received NAMED_VALUE_INT or NAMED_VALUE_FLOAT")
                self.process_and_write_received_named_value(msg_dict)

            elif msg_type == mavlinkhelper.MSG_TYPE_STATUSTEXT:
                log.info("State.NEUTRAL: received STATUSTEXT")
                self.process_and_write_received_statustext(msg_dict)

            elif msg_type == mavlinkhelper.MSG_TYPE_DEBUG:
                log.info("State.NEUTRAL: received DEBUG")
                log.debug("writing DEBUG packet is not implemented")

            else:
                log.info("State.NEUTRAL: did receive %s", msg_type)

        elif self.__state == State.DATA_TRANSMISSION_HANDSHAKE:

            if msg_type == mavlinkhelper.MSG_TYPE_DATA_TRANSMISSION_HANDSHAKE:
                # double handshake : no easy way to decide if / when the old
                # handshaked transmission will be / is / was finished
                # => new handshake is OK , discard received old handshake and
                # data transmission packets
                log.warn(
                    "State.DATA_TRANSMISSION_HANDSHAKE: received DATA_TRANSMISSION_HANDSHAKE"
                    " -> unexpected => discarding old handshake and transmitted data")
                self.__state = State.DATA_TRANSMISSION_HANDSHAKE
                self.__data_transmission_handshake_dict = msg_dict
                self.__encapsulated_data_dicts = []

            elif msg_type == mavlinkhelper.MSG_TYPE_ENCAPSULATED_DATA:
                log.info("State.DATA_TRANSMISSION_HANDSHAKE: received ENCAPSULATED_DATA")
                self.__encapsulated_data_dicts.append(msg_dict)
                self.__state, imgdata = self.process_and_write_received_encapsulated_data()

            elif msg_type == mavlinkhelper.MSG_TYPE_NAMED_VALUE_INT or \
                    msg_type == mavlinkhelper.MSG_TYPE_NAMED_VALUE_FLOAT:
                log.info("State.DATA_TRANSMISSION_HANDSHAKE: received NAMED_VALUE_INT or NAMED_VALUE_FLOAT")
                self.process_and_write_received_named_value(msg_dict)

            elif msg_type == mavlinkhelper.MSG_TYPE_STATUSTEXT:
                log.info("State.DATA_TRANSMISSION_HANDSHAKE: received STATUSTEXT")
                self.process_and_write_received_statustext(msg_dict)

            elif msg_type == mavlinkhelper.MSG_TYPE_DEBUG:
                log.info("State.DATA_TRANSMISSION_HANDSHAKE: received DEBUG")
                log.debug("writing DEBUG packet is not implemented")

            else:
                log.info("State.DATA_TRANSMISSION_HANDSHAKE: did receive %s", msg_type)
