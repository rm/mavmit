#
# This file is part of mavmit <https://gitlab.com/rm/mavmit>.
# mavmit allows to transmit files via mavlink connections and to measure connection quality
#
# Copyright (C) 2021 Richard Meinsen
#
# mavmit is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# navmit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#


import time


def epoch_s():
    return time.time()


def epoch_ms():
    return time.time() * 1.0e3


def epoch_us():
    return time.time() * 1.0e6


def epoch_ns():
    return time.time() * 1.0e9


def epoch_s_int():
    return int(time.time())


def epoch_ms_int():
    return int(time.time() * 1.0e3)


def epoch_us_int():
    return int(time.time() * 1.0e6)


def epoch_ns_int():
    return int(time.time() * 1.0e9)


def us_to_ms(us):
    return us / 1000


def ns_to_us(ns):
    return ns / 1000
