#
# This file is part of mavmit <https://gitlab.com/rm/mavmit>.
# mavmit allows to transmit files via mavlink connections and to measure connection quality
#
# Copyright (C) 2021 Richard Meinsen
#
# mavmit is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# navmit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#


import codecs
import os
import sys

from PIL import Image
from watchdog.events import RegexMatchingEventHandler

from mavmit import applogging
from mavmit import mavlinkhelper
from mavmit import mavmitfieldnames as MFN
from mavmit import timehelper

log = applogging.get_applogger()

IMAGEFILEEXTENSION_JPG = 'jpg'
IMAGEFILEEXTENSION_JPEG = 'jpeg'
IMAGEFILEEXTENSION_BMP = 'bmp'
IMAGEFILEEXTENSION_PNG = 'png'
IMAGEFILEEXTENSIONS = [
    IMAGEFILEEXTENSION_JPG,
    IMAGEFILEEXTENSION_JPEG,
    IMAGEFILEEXTENSION_BMP,
    IMAGEFILEEXTENSION_PNG,
]
IMAGEFILES_REGEX = r".*\.(?:{}|{}|{}|{})$".format(
    IMAGEFILEEXTENSION_JPG, IMAGEFILEEXTENSION_JPEG,
    IMAGEFILEEXTENSION_BMP, IMAGEFILEEXTENSION_PNG)

RAWDATAFILEEXTENSION_RAW8U = 'raw8u'
RAWDATAFILEEXTENSION_RAW32U = 'raw32u'
RAWDATAFILEEXTENSIONS = [
    RAWDATAFILEEXTENSION_RAW8U,
    RAWDATAFILEEXTENSION_RAW32U
]
RAWDATAFILES_REGEX = r".*\.(?:{}|{})$".format(
    RAWDATAFILEEXTENSION_RAW8U, RAWDATAFILEEXTENSION_RAW32U)

NAMEDVALUE_FILEEXTENSION_INT = 'namedvalueint'
NAMEDVALUE_FILEEXTENSION_FLOAT = 'namedvaluefloat'
NAMEDVALUE_FILEEXTENSIONS = [
    NAMEDVALUE_FILEEXTENSION_INT,
    NAMEDVALUE_FILEEXTENSION_FLOAT
]
NAMEDVALUEFILES_REGEX = r".*\.(?:{}|{})$".format(
    NAMEDVALUE_FILEEXTENSION_INT,
    NAMEDVALUE_FILEEXTENSION_FLOAT)

STATUSTEXT_FILEEXTENSION = 'statustext'
STATUSTEXT_FILEEXTENSIONS = [
    STATUSTEXT_FILEEXTENSION,
]

STATUSTEXT_FILEEXTENSION_REGEX = r".*\.(?:{})$".format(
    STATUSTEXT_FILEEXTENSION)

FILES_REGEX = [
    IMAGEFILES_REGEX,
    RAWDATAFILES_REGEX,
    NAMEDVALUEFILES_REGEX,
    STATUSTEXT_FILEEXTENSION_REGEX
]


class FileHandler(RegexMatchingEventHandler):

    # See https://github.com/gorakhargosh/watchdog/blob/master/src/watchdog/events.py
    #
    # class FileSystemEventHandler:
    #
    # class PatternMatchingEventHandler(FileSystemEventHandler):
    #   Matches given patterns with file paths associated with occurring events.
    #
    # class RegexMatchingEventHandler(FileSystemEventHandler):
    #   Matches given regexes with file paths associated with occurring events.
    #
    # class LoggingEventHandler(FileSystemEventHandler):
    #   Logs all the events captured.
    #

    def __init__(self, send_queues):
        super().__init__(FILES_REGEX)
        self.__send_queues = send_queues

    # All event objects have the attributes
    # - event_type,
    # - is_directory
    # - src_path.

    def on_any_event(self, event):  # catch-all file system events
        pass

    def on_created(self, event):  # called when a file or directory is created
        pass

    def on_deleted(self, event):  # called when a file or directory is deleted
        pass

    def on_modified(self, event):  # called when a file or directory is changed
        pass

    def on_moved(self, event):  # called when a file or a directory is moved or renamed
        pass

    def on_closed(self, event):  # called when a file or a directory is closed
        log.debug("event: close file %s", event.src_path)
        self.process(event)

    def process(self, event):
        if os.path.isfile(event.src_path):
            filename, ext = os.path.splitext(event.src_path)
            if ext:
                ext = ext[1:]

            if ext in NAMEDVALUE_FILEEXTENSIONS:
                self.__send_queues.put_datafile_name(event.src_path)

            elif ext in STATUSTEXT_FILEEXTENSIONS:
                self.__send_queues.put_datafile_name(event.src_path)

            elif ext in IMAGEFILEEXTENSIONS:
                self.__send_queues.put_imagefile_name(event.src_path)

            elif ext in RAWDATAFILEEXTENSIONS:
                self.__send_queues.put_imagefile_name(event.src_path)

            else:
                log.warn("no valid queue for file: %s", event.src_path)

        else:
            log.info("closing not a file: %s", event.src_path)

    def process_file(self, src_path):
        try:
            log.debug("processing file %s", src_path)

            filename, ext = os.path.splitext(src_path)
            if ext:
                ext = ext[1:]
            if ext in IMAGEFILEEXTENSIONS or ext in RAWDATAFILEEXTENSIONS:
                log.info("handling file %s", src_path)
                if ext in IMAGEFILEEXTENSIONS:
                    image = Image.open(src_path)
                    image_width, image_height = image.size
                    log.info("image file %s dimensions %d , %d", src_path, image_width, image_height)
                    image.close()
                else:
                    image_width = 1
                    image_height = os.path.getsize(src_path)
                    log.info("raw data file %s size 1 x %d", src_path, image_height)

                allpackets = []
                with open(src_path, "rb") as file:
                    imagepacket_dicts = []
                    image_byte_count = 0
                    imagepacket_seqnum = 0

                    while True:
                        # https://mavlink.io/en/messages/common.html#ENCAPSULATED_DATA
                        # seqnr	uint16_t	sequence number (starting with 0 on every transmission)
                        # data	uint8_t[253]	image data bytes

                        bytes_read = file.read(mavlinkhelper.MAVLINK_ENCAPSULATED_DATA_PACKETSIZE_BYTES)
                        if bytes_read != b"":
                            read_bytes_count = len(bytes_read)
                            image_byte_count += read_bytes_count
                            msg_dict = dict()
                            msg_dict[MFN.MSGDICT_MSG_TYPE] = mavlinkhelper.MSG_TYPE_ENCAPSULATED_DATA
                            msg_dict[MFN.MSGDICT_MSG_SEQ] = imagepacket_seqnum

                            log.debug("read bytes %d", read_bytes_count)
                            if read_bytes_count < mavlinkhelper.MAVLINK_ENCAPSULATED_DATA_PACKETSIZE_BYTES:
                                # THIS IS NOT AS I EXPECTED
                                # one has to send padded packets ...
                                padding_len = mavlinkhelper.MAVLINK_ENCAPSULATED_DATA_PACKETSIZE_BYTES \
                                              - read_bytes_count
                                bytes_read_extend = bytearray(bytes_read)
                                bytes_padding = bytes(padding_len)
                                bytes_read_extend.extend(bytes_padding)
                                msg_dict[MFN.MSGDICT_DATA] = bytes_read_extend
                                msg_dict[MFN.MSGDICT_DATA_HEXENCODED] = \
                                    codecs.encode(bytearray(bytes_read_extend), "hex").decode('ascii')
                                log.debug("prepared ENCAPSULATED_DATA with %d bytes data, %d bytes padding",
                                          read_bytes_count, padding_len)
                            else:
                                log.debug("prepared ENCAPSULATED_DATA with %d bytes data", read_bytes_count)
                                msg_dict[MFN.MSGDICT_DATA] = bytes_read
                                msg_dict[MFN.MSGDICT_DATA_HEXENCODED] = \
                                    codecs.encode(bytearray(bytes_read), "hex").decode('ascii')
                            # Option: add MSGDICT_INPUTFILE_TIMESTAMP_MS
                            msg_dict[MFN.MSGDICT_PACKET_CREATE_TIMESTAMP_US] = \
                                timehelper.epoch_us()

                            imagepacket_seqnum += 1
                            imagepacket_dicts.append(msg_dict)
                        else:
                            break

                    log.debug("file read into %d packets", imagepacket_seqnum)

                    if len(imagepacket_dicts) > 0:
                        # got image data  -> prepend leading packet
                        # https://mavlink.io/en/messages/common.html#DATA_TRANSMISSION_HANDSHAKE
                        # type	uint8_t		        MAVLINK_DATA_STREAM_TYPE
                        #                           Type of requested/acknowledged data.
                        # size	uint32_t			total data size in bytes (set on ACK only).
                        # width	uint16_t			Width of a matrix or image.
                        # height	uint16_t        Height of a matrix or image.
                        # packets	uint16_t		Number of packets being sent (set on ACK only).
                        # payload	uint8_t	bytes	Payload size per packet  (normally 253 byte,
                        #                           see DATA field size in message ENCAPSULATED_DATA)
                        #                           (set on ACK only).
                        # jpg_quality	uint8_t	%	JPEG quality. Values: [1-100].
                        msg_dict = dict()
                        msg_dict[MFN.MSGDICT_MSG_TYPE] = \
                            mavlinkhelper.MSG_TYPE_DATA_TRANSMISSION_HANDSHAKE

                        img_type = mavlinkhelper.stream_type_from_ext(ext)
                        if img_type is None:
                            log.error("invalid file extension: %s", ext)
                        else:
                            msg_dict[MFN.MSGDICT_MAVLINK_DATA_STREAM_TYPE] = img_type
                            msg_dict[MFN.MSGDICT_IMAGE_WIDTH] = image_width
                            msg_dict[MFN.MSGDICT_IMAGE_HEIGHT] = image_height
                            msg_dict[MFN.MSGDICT_IMAGE_JPG_QUALITY] = 100  # how to define jpg quality ...
                            msg_dict[MFN.MSGDICT_BYTES_NUM] = image_byte_count
                            msg_dict[MFN.MSGDICT_PACKETS_NUM] = imagepacket_seqnum
                            msg_dict[MFN.MSGDICT_PACKET_PAYLOAD_BYTES] = \
                                mavlinkhelper.MAVLINK_ENCAPSULATED_DATA_PACKETSIZE_BYTES

                            # Option: add MSGDICT_INPUTFILE_TIMESTAMP_MS
                            msg_dict[MFN.MSGDICT_PACKET_CREATE_TIMESTAMP_US] = \
                                timehelper.epoch_us()

                            allpackets.append(msg_dict)
                            allpackets.extend(imagepacket_dicts)

                    else:
                        log.error("could not build images packets")

                if len(allpackets) > 0:
                    self.__send_queues.put_packets(allpackets)

                    # Option: move file to outbox_s / remove file / ..
                else:
                    log.error("could not build list of packages to send")

            elif ext in NAMEDVALUE_FILEEXTENSIONS:
                log.info("handling named value file %s", src_path)

                # https://mavlink.io/en/messages/common.html#NAMED_VALUE_INT
                # MSG_TYPE_NAMED_VALUE_INT = "NAMED_VALUE_INT"
                # Field Name	Type	    Units	Description
                # time_boot_ms	uint32_t	ms	    Timestamp (time since system boot).
                # name	        char[10]		    Name of the debug variable
                # value	        int32_t		        Signed integer value

                with open(src_path, "r") as file:
                    line = file.readline()
                    name = None

                    if line is not None and len(line) > 3:
                        parts = line.split(":")
                        if parts[0] is not None and len(parts[0].strip()) > 0:
                            name = parts[0].strip()
                        else:
                            log.error("named value file does not contain valid name."
                                      "\ncontent is: %s", src_path, line)

                        value = None
                        packet = dict()
                        if parts[1] is not None and len(parts[1].strip()) > 0:
                            if ext == NAMEDVALUE_FILEEXTENSION_INT:
                                try:
                                    value = int(parts[1])
                                    # Option: add MSGDICT_INPUTFILE_TIMESTAMP_MS
                                    packet[MFN.MSGDICT_PACKET_CREATE_TIMESTAMP_US] = \
                                        timehelper.epoch_us()
                                    packet[MFN.MSGDICT_MSG_TYPE] = \
                                        mavlinkhelper.MSG_TYPE_NAMED_VALUE_INT
                                    packet[MFN.MSGDICT_NAMED_VALUE_NAME] = name
                                    packet[MFN.MSGDICT_NAMED_VALUE_INT_VALUE] = value
                                except ValueError:
                                    log.error("named value file %s does not contain valid int value."
                                              "\ncontent is: %s",
                                              src_path, line)

                            if ext == NAMEDVALUE_FILEEXTENSION_FLOAT:
                                try:
                                    value = float(parts[1])
                                    # Option: add MSGDICT_INPUTFILE_TIMESTAMP_US/MS
                                    packet[MFN.MSGDICT_PACKET_CREATE_TIMESTAMP_US] = \
                                        timehelper.epoch_us()
                                    packet[MFN.MSGDICT_MSG_TYPE] = \
                                        mavlinkhelper.MSG_TYPE_NAMED_VALUE_FLOAT
                                    packet[MFN.MSGDICT_NAMED_VALUE_NAME] = name
                                    packet[MFN.MSGDICT_NAMED_VALUE_FLOAT_VALUE] = value
                                except ValueError:
                                    log.error("named value file %s does not contain valid float value."
                                              "\ncontent is: %s",
                                              src_path, line)

                        if name and value:
                            self.__send_queues.put_packet(packet)
                    else:
                        log.error("named value file %s does not contain valid content: %s", src_path, line)

            elif ext in STATUSTEXT_FILEEXTENSIONS:
                log.info("handling statustext file %s", src_path)

                # MSG_TYPE_STATUSTEXT = "STATUSTEXT"  # https://mavlink.io/en/messages/common.html#STATUSTEXT
                # severity	uint8_t	MAV_SEVERITY	Severity of status. Relies on the definitions within RFC-5424.
                # text	char[50]		Status text message, without null termination character
                # id **	uint16_t		Unique (opaque) identifier for this statustext message.
                #   May be used to reassemble a logical long-statustext message from a sequence of chunks.
                #   A value of zero indicates this is the only chunk in the sequence and the message can be
                #   emitted immediately.
                # chunk_seq **          This chunk's sequence number; indexing is from zero. Any null character
                #                       in the text field is taken to mean this was the last chunk.
                #
                # pymavlink doesn't support id and chunk_seq
                # def statustext_send(self, severity, text, force_mavlink1=False)

                with open(src_path, "r") as file:
                    line = file.readline()
                    statustext_text = None
                    statustext_severity = None

                    if line is not None and len(line) > 3:
                        parts = line.split(":")
                        if len(parts) == 2:

                            if parts[0] is not None and len(parts[0].strip()) > 0:
                                statustext_text = parts[0].strip()
                            else:
                                log.error("statustext file %s does not contain valid statustext."
                                          "\ncontent is: %s",
                                          src_path, line)

                            if parts[1] is not None and len(parts[1].strip()) > 0:
                                try:
                                    statustext_severity = int(parts[1].strip())

                                    # https://mavlink.io/en/messages/common.html#MAV_SEVERITY
                                    if not 0 <= statustext_severity <= 7:
                                        statustext_severity = None
                                        log.error(
                                            "statustext file %s does not contain severity "
                                            "in valid range 0..7."
                                            "\ncontent is: %s",
                                            src_path, line)

                                except Exception:
                                    log.error("statustext file %s does not contain valid severity."
                                              "\ncontent is: %s",
                                              src_path, line)

                        else:
                            log.error("statustext file %s does not contain text and severity."
                                      "\ncontent is: %s",
                                      src_path, line)
                    else:
                        log.error("statustext file %s does not valid content."
                                  "\ncontent is: %s",
                                  src_path, line)

                    if statustext_severity is not None and statustext_text is not None:
                        log.debug("putting statustext packet in send queue")
                        packet = dict()
                        packet[MFN.MSGDICT_MSG_TYPE] = mavlinkhelper.MSG_TYPE_STATUSTEXT
                        packet[MFN.MSGDICT_STATUSTEXT_TEXT] = statustext_text
                        packet[MFN.MSGDICT_STATUSTEXT_SEVERITY] = statustext_severity
                        # Option: add MSGDICT_INPUTFILE_TIMESTAMP_MS
                        packet[MFN.MSGDICT_PACKET_CREATE_TIMESTAMP_US] = \
                            timehelper.epoch_us()

                        self.__send_queues.put_packet(packet)

            else:
                log.error("Unexpected file extension %s for file %s", ext, src_path)
                # MSG_TYPE_DEBUG = "DEBUG"  # https://mavlink.io/en/messages/common.html#DEBUG

        except Exception as e:
            log.error("Filehandler processing file -> unexpected error %s,%s", sys.exc_info()[0], e)
