#
# This file is part of mavmit <https://gitlab.com/rm/mavmit>.
# mavmit allows to transmit files via mavlink connections and to measure connection quality
#
# Copyright (C) 2021 Richard Meinsen
#
# mavmit is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# navmit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#


import os.path
import sys

from mavmit import applogging
from mavmit import handleargs
from mavmit import mavlinkhelper
from mavmit.dispatcher import Dispatcher
from mavmit.sendqueues import SendQueues

if __name__ == "__main__":
    args = handleargs.handle_args()
    log = applogging.configure_applogging(args.logfile)

    if not os.path.isdir(args.inputdir):
        log.error("invalid parameter value for inputdir: %s", args.inputdir)
        sys.exit(os.EX_CONFIG)
    elif not os.path.isdir(args.outputdir):
        log.error("invalid parameter value for outputdir: %s", args.outputdir)
        sys.exit(os.EX_CONFIG)

    mavlink_connection = mavlinkhelper.connect_mavlink(args)
    if not mavlink_connection:
        log.error("could not establish mavlink connection to: %s", args.mavlinkDevice)
        sys.exit(os.EX_IOERR)

    sendQueues = SendQueues([], [], [])
    Dispatcher(args, sendQueues, mavlink_connection).run()
