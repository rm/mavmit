#
# This file is part of mavmit <https://gitlab.com/rm/mavmit>.
# mavmit allows to transmit files via mavlink connections and to measure connection quality
#
# Copyright (C) 2021 Richard Meinsen
#
# mavmit is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# navmit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#


import codecs
import time

from pymavlink import mavutil

from mavmit import appconfig as CNF
from mavmit import applogging
from mavmit import mavmitfieldnames as MFN
from mavmit import timehelper

MAVLINK_ENCAPSULATED_DATA_PACKETSIZE_BYTES = 253

MSG_TYPE_UNKNOWN \
    = "MAVLINK_MSG_TYPE_UNKNOWN"

MSG_TYPE_RADIO_STATUS = \
    "RADIO_STATUS"  # https://mavlink.io/en/messages/common.html#RADIO_STATUS

MSG_TYPE_HEARTBEAT = \
    "HEARTBEAT"  # https://mavlink.io/en/messages/common.html#HEARTBEAT

MSG_TYPE_PING = \
    "PING"  # https://mavlink.io/en/messages/common.html#PING

MSG_TYPE_SYSTEM_TIME = \
    "SYSTEM_TIME"  # https://mavlink.io/en/messages/common.html#SYSTEM_TIME

MSG_TYPE_DATA_TRANSMISSION_HANDSHAKE \
    = "DATA_TRANSMISSION_HANDSHAKE"  # https://mavlink.io/en/messages/common.html#DATA_TRANSMISSION_HANDSHAKE
MSG_TYPE_ENCAPSULATED_DATA \
    = "ENCAPSULATED_DATA"  # https://mavlink.io/en/messages/common.html#ENCAPSULATED_DATA

MSG_TYPE_NAMED_VALUE_FLOAT = \
    "NAMED_VALUE_FLOAT"  # https://mavlink.io/en/messages/common.html#NAMED_VALUE_FLOAT
MSG_TYPE_NAMED_VALUE_INT = \
    "NAMED_VALUE_INT"  # https://mavlink.io/en/messages/common.html#NAMED_VALUE_INT

MSG_TYPE_STATUSTEXT = \
    "STATUSTEXT"  # https://mavlink.io/en/messages/common.html#STATUSTEXT

MSG_TYPE_DEBUG = \
    "DEBUG"  # https://mavlink.io/en/messages/common.html#DEBUG
MSG_TYPE_DEBUG_VECT = \
    "DEBUG_VECT"  # https://mavlink.io/en/messages/common.html#DEBUG_VECT
MSG_TYPE_MEMORY_VECT = \
    "MEMORY_VECT"  # https://mavlink.io/en/messages/common.html#MEMORY_VECT

MSG_TYPE_PLAY_TUNE_V2 = \
    "PLAY_TUNE_V2"  # https://mavlink.io/en/messages/common.html#PLAY_TUNE_V2

MSG_TYPE_V2_EXTENSION = \
    "V2_EXTENSION"  # https://mavlink.io/en/messages/common.html#V2_EXTENSION

MSG_ATTRIBUTE_RADIO_STATUS_RSSI = "rssi"
MSG_ATTRIBUTE_RADIO_STATUS_REMRSSI = "remrssi"
MSG_ATTRIBUTE_RADIO_STATUS_TXBUF = "txbuf"
MSG_ATTRIBUTE_RADIO_STATUS_NOISE = "noise"
MSG_ATTRIBUTE_RADIO_STATUS_REMNOISE = "remnoise"
MSG_ATTRIBUTE_RADIO_STATUS_RXERRORS = "rxerrors"
MSG_ATTRIBUTE_RADIO_STATUS_FIXED = "fixed"

MSG_ATTRIBUTE_NAME = "name"
MSG_ATTRIBUTE_TYPE = "type"
MSG_ATTRIBUTE_WIDTH = "width"
MSG_ATTRIBUTE_HEIGHT = "height"
MSG_ATTRIBUTE_JPG_QUALITY = "jpg_quality"
MSG_ATTRIBUTE_SIZE = "size"
MSG_ATTRIBUTE_PACKETS = "packets"
MSG_ATTRIBUTE_PAYLOAD = "payload"
MSG_ATTRIBUTE_SEVERITY = "severity"
MSG_ATTRIBUTE_TEXT = "text"
MSG_ATTRIBUTE_TIME_BOOT_MS = "time_boot_ms"
MSG_ATTRIBUTE_VALUE = "value"
MSG_ATTRIBUTE_DATA = "data"  # msg['data']
MSG_ATTRIBUTE_PARAM_ID = "param_id"  # msg['param_id'].decode("utf-8")
MSG_ATTRIBUTE_PARAM_VALUE = "param_value"  # msg['param_value']

log = applogging.get_applogger()


def ext_from_stream_type(stream_type):
    extension = [
        MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_JPEG_EXT,
        MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_BMP_EXT,
        MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_RAW8U_EXT,
        MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_RAW32U_EXT,
        MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_PGM_EXT,
        MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_PNG_EXT
    ]
    if MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_JPEG <= stream_type <= MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_PNG:
        return extension[stream_type]
    else:
        return None


def stream_type_from_ext(ext):
    if ext == MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_PNG_EXT:
        return MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_PNG

    elif ext == MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_JPG_EXT or \
            ext == MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_JPEG_EXT:
        return MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_JPEG

    elif ext == MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_BMP_EXT:
        return MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_BMP

    elif ext == MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_PGM_EXT:
        return MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_PGM

    elif ext == MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_RAW8U_EXT:
        return MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_RAW8U

    elif ext == MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_RAW32U_EXT:
        return MFN.MSGDICT_MAVLINK_DATA_STREAM_IMG_RAW32U

    else:
        log.error("could not determine mavlink data stream type for file extension: %s", ext)
        return None


def epoch_us_and_time_since_boot_ms(args):
    timestamp_us = timehelper.epoch_us()
    timestamp_ms = timehelper.us_to_ms(timestamp_us)
    time_since_boot_ms = timestamp_ms - args.boottime_ms
    return timestamp_us, time_since_boot_ms


def msg_to_msg_dict_tagged_recv(msg):
    msg_dict = _msg_to_msgdict(msg)
    msg_dict[MFN.MSGDICT_TAG_DIRECTION] = MFN.MSGDICT_TAG_DIRECTION_VALUE_RECV
    return msg_dict


def msg_to_data_dict_tag_send(msg_dict):
    msg_dict[MFN.MSGDICT_TAG_DIRECTION] = MFN.MSGDICT_TAG_DIRECTION_VALUE_SEND
    return msg_dict


def _msg_to_msgdict(msg):
    recv_timestamp_us = timehelper.epoch_us()
    msg_type = None
    try:
        msg_type = msg.get_type()
    except AttributeError:
        log.debug("received message where get_type doesn't work")

    if msg_type is None:
        try:
            name = msg.__getattribute__(MSG_ATTRIBUTE_NAME)
            if name == MSG_TYPE_RADIO_STATUS or name == MSG_TYPE_HEARTBEAT:
                msg_type = name
            else:
                log.info("received message without expected type")
                msg_type = MSG_TYPE_UNKNOWN
        except Exception:
            log.warning("received message without known type and without attribute 'name'")

    msg_id = msg.get_msgId()
    msg_seq = msg.get_seq()
    msg_buf = msg.get_msgbuf()
    msg_buf_encoded = codecs.encode(bytearray(msg_buf), "hex").decode('ascii')
    src_sys = msg.get_srcSystem()
    src_comp = msg.get_srcComponent()
    msg_signed = msg.get_signed()
    fieldnames = msg.get_fieldnames()
    mavlink_msg_dict = msg.to_dict()
    msg_crc = msg._crc  # Other option? not in to_dict()
    # msg_timestamp_epoch_s = msg._timestamp  # Other option? not in to_dict()
    # internally set by  time.time() ~ seconds since the epoch as a floating point number

    # Option: put whole original dict into msg_dict?

    msgdict = {
        MFN.MSGDICT_TAG_DIRECTION: MFN.MSGDICT_TAG_DIRECTION_VALUE_RECV,
        # MFN.MSGDICT_MSG_TIMESTAMP_S: msg_timestamp_epoch_s,
        MFN.MSGDICT_MSG_TYPE: msg_type,
        MFN.MSGDICT_MSG_ID: msg_id,
        MFN.MSGDICT_MSG_SEQ: msg_seq,
        MFN.MSGDICT_MSG_SIGNED: msg_signed,
        MFN.MSGDICT_MSG_CRC: msg_crc,
        MFN.MSGDICT_SRCSYSTEM: src_sys,
        MFN.MSGDICT_SRCCOMPONENT: src_comp,
        MFN.MSGDICT_MSG_BUF: msg_buf,
        MFN.MSGDICT_MSG_BUF_HEXENCODED: msg_buf_encoded,
        MFN.MSGDICT_RECV_TIMESTAMP_US: recv_timestamp_us
    }

    if msg.get_link_id():
        msgdict[MFN.MSGDICT_LINK_ID] = msg.get_link_id()
    else:
        msgdict[MFN.MSGDICT_LINK_ID] = ""

    if msg_type == MSG_TYPE_DATA_TRANSMISSION_HANDSHAKE:
        log.debug("received %s", MSG_TYPE_DATA_TRANSMISSION_HANDSHAKE)
        try:
            msgdict[MFN.MSGDICT_MAVLINK_DATA_STREAM_TYPE] = msg.__getattribute__(MSG_ATTRIBUTE_TYPE)
            msgdict[MFN.MSGDICT_IMAGE_WIDTH] = msg.__getattribute__(MSG_ATTRIBUTE_WIDTH)
            msgdict[MFN.MSGDICT_IMAGE_HEIGHT] = msg.__getattribute__(MSG_ATTRIBUTE_HEIGHT)
            msgdict[MFN.MSGDICT_IMAGE_JPG_QUALITY] = msg.__getattribute__(MSG_ATTRIBUTE_JPG_QUALITY)
            msgdict[MFN.MSGDICT_BYTES_NUM] = msg.__getattribute__(MSG_ATTRIBUTE_SIZE)
            msgdict[MFN.MSGDICT_PACKETS_NUM] = msg.__getattribute__(MSG_ATTRIBUTE_PACKETS)
            msgdict[MFN.MSGDICT_PACKET_PAYLOAD_BYTES] = msg.__getattribute__(MSG_ATTRIBUTE_PAYLOAD)
        except Exception:
            log.warning("could not decode DATA_TRANSMISSION_HANDSHAKE")
            return None

    elif msg_type == MSG_TYPE_ENCAPSULATED_DATA:
        log.debug("received %s", MSG_TYPE_ENCAPSULATED_DATA)
        if MSG_ATTRIBUTE_DATA in mavlink_msg_dict:
            if isinstance(mavlink_msg_dict[MSG_ATTRIBUTE_DATA], list):
                encoded = codecs.encode(bytearray(mavlink_msg_dict[MSG_ATTRIBUTE_DATA]), "hex").decode('ascii')
            else:
                encoded = codecs.encode(mavlink_msg_dict[MSG_ATTRIBUTE_DATA], "hex").decode('ascii')
            msgdict[MFN.MSGDICT_DATA_HEXENCODED] = encoded
            msgdict[MFN.MSGDICT_DATA] = mavlink_msg_dict[MSG_ATTRIBUTE_DATA]

    elif msg_type == MSG_TYPE_STATUSTEXT:
        log.debug("received %s", MSG_TYPE_STATUSTEXT)
        msgdict[MFN.MSGDICT_STATUSTEXT_SEVERITY] = msg.__getattribute__(MSG_ATTRIBUTE_SEVERITY)
        msgdict[MFN.MSGDICT_STATUSTEXT_TEXT] = msg.__getattribute__(MSG_ATTRIBUTE_TEXT)

    elif msg_type == MSG_TYPE_NAMED_VALUE_FLOAT:
        log.debug("received %s", MSG_TYPE_NAMED_VALUE_FLOAT)
        msgdict[MFN.MSGDICT_TIME_SINCE_BOOT_MS] = msg.__getattribute__(MSG_ATTRIBUTE_TIME_BOOT_MS)
        msgdict[MFN.MSGDICT_NAMED_VALUE_NAME] = msg.__getattribute__(MSG_ATTRIBUTE_NAME)
        msgdict[MFN.MSGDICT_NAMED_VALUE_FLOAT_VALUE] = msg.__getattribute__(MSG_ATTRIBUTE_VALUE)

    elif msg_type == MSG_TYPE_NAMED_VALUE_INT:
        log.debug("received %s", MSG_TYPE_NAMED_VALUE_INT)
        msgdict[MFN.MSGDICT_TIME_SINCE_BOOT_MS] = msg.__getattribute__(MSG_ATTRIBUTE_TIME_BOOT_MS)
        msgdict[MFN.MSGDICT_NAMED_VALUE_NAME] = msg.__getattribute__(MSG_ATTRIBUTE_NAME)
        msgdict[MFN.MSGDICT_NAMED_VALUE_INT_VALUE] = msg.__getattribute__(MSG_ATTRIBUTE_VALUE)

    elif msg_type == MSG_TYPE_RADIO_STATUS:
        log.debug("received %s", MSG_TYPE_RADIO_STATUS)
        msgdict[MFN.MSGDICT_MSG_ATTRIBUTE_RADIO_STATUS_TXBUF] = \
            msg.__getattribute__(MSG_ATTRIBUTE_RADIO_STATUS_TXBUF)
        msgdict[MFN.MSGDICT_MSG_ATTRIBUTE_RADIO_STATUS_REMRSSI] = \
            msg.__getattribute__(MSG_ATTRIBUTE_RADIO_STATUS_REMRSSI)
        msgdict[MFN.MSGDICT_MSG_ATTRIBUTE_RADIO_STATUS_REMNOISE] = \
            msg.__getattribute__(MSG_ATTRIBUTE_RADIO_STATUS_REMNOISE)
        msgdict[MFN.MSGDICT_MSG_ATTRIBUTE_RADIO_STATUS_RSSI] = \
            msg.__getattribute__(MSG_ATTRIBUTE_RADIO_STATUS_RSSI)
        msgdict[MFN.MSGDICT_MSG_ATTRIBUTE_RADIO_STATUS_NOISE] = \
            msg.__getattribute__(MSG_ATTRIBUTE_RADIO_STATUS_NOISE)
        msgdict[MFN.MSGDICT_MSG_ATTRIBUTE_RADIO_STATUS_FIXED] = \
            msg.__getattribute__(MSG_ATTRIBUTE_RADIO_STATUS_RXERRORS)
        msgdict[MFN.MSGDICT_MSG_ATTRIBUTE_RADIO_STATUS_FIXED] = \
            msg.__getattribute__(MSG_ATTRIBUTE_RADIO_STATUS_FIXED)

    else:
        pass

    return msgdict


def send_ping(mavlink_connection, args, seq_ping_number):
    timestamp_us = timehelper.epoch_us()

    msg_dict = {
        MFN.MSGDICT_TAG_DIRECTION: MFN.MSGDICT_TAG_DIRECTION_VALUE_SEND,
        MFN.MSGDICT_SEND_TIMESTAMP_US: timestamp_us,
        MFN.MSGDICT_MSG_TYPE: MSG_TYPE_PING,
        MFN.MSGDICT_SEQ_PING_NUMBER: seq_ping_number,
        MFN.MSGDICT_SRCSYSTEM: args.srcSystem,
        MFN.MSGDICT_SRCCOMPONENT: args.srcComponent,
        MFN.MSGDICT_TARGETSYSTEM: args.targetSystem,
        MFN.MSGDICT_TARGETCOMPONENT: args.targetComponent
    }

    # ping_send(self, time_usec, seq, target_system, target_component, force_mavlink1=False):
    mavlink_connection.mav.ping_send(
        timestamp_us,
        seq_ping_number,
        args.targetSystem,  # 0 ~ Request ping of all systems
        args.targetComponent  # 0 ~ Request ping of all components
    )
    return msg_dict


def send_systemtime(mavlink_connection, args, seq_ping_number):
    time_since_boot_ms, timestamp_us = epoch_us_and_time_since_boot_ms(args)

    msg_dict = {
        MFN.MSGDICT_TAG_DIRECTION: MFN.MSGDICT_TAG_DIRECTION_VALUE_SEND,
        MFN.MSGDICT_MSG_TYPE: MSG_TYPE_SYSTEM_TIME,
        MFN.MSGDICT_SEND_TIMESTAMP_US: timestamp_us,
        MFN.MSGDICT_TIME_SINCE_BOOT_MS: time_since_boot_ms,
        MFN.MSGDICT_SRCSYSTEM: args.srcSystem,
        MFN.MSGDICT_SRCCOMPONENT: args.srcComponent
    }

    # def system_time_send(self, time_unix_usec, time_boot_ms, force_mavlink1=False):
    mavlink_connection.mav.system_time_send(
        timestamp_us,
        time_since_boot_ms
    )
    return msg_dict


def send_heartbeat(mavlink_connection, args):
    """
    https://mavlink.io/en/mavgen_python/#heartbeat
    # Send heartbeat from a GCS (types are define as enum in the dialect file).
    the_connection.mav.heartbeat_send(mavutil.mavlink.MAV_TYPE_GCS,
                                      mavutil.mavlink.MAV_AUTOPILOT_INVALID, 0, 0, 0)

    # Send heartbeat from a MAVLink application.
    the_connection.mav.heartbeat_send(mavutil.mavlink.MAV_TYPE_ONBOARD_CONTROLLER,
                                      mavutil.mavlink.MAV_AUTOPILOT_INVALID, 0, 0, 0)

    type              : Type of the MAV (quadrotor, helicopter, etc.) (type:uint8_t, values:MAV_TYPE)
    autopilot         : Autopilot type / class. (type:uint8_t, values:MAV_AUTOPILOT)
    base_mode         : System mode bitmap. (type:uint8_t, values:MAV_MODE_FLAG)
    custom_mode       : A bitfield for use for autopilot-specific flags (type:uint32_t)
    system_status     : System status flag. (type:uint8_t, values:MAV_STATE)
    mavlink_version   : MAVLink version, not writable by user, gets added by protocol because of magic data type:
                            uint8_t_mavlink_version (type:uint8_t)
    """

    timestamp_us = timehelper.epoch_us()
    datadict = {
        MFN.MSGDICT_TAG_DIRECTION: MFN.MSGDICT_TAG_DIRECTION_VALUE_SEND,
        MFN.MSGDICT_MSG_TYPE: MSG_TYPE_HEARTBEAT,
        MFN.MSGDICT_SEND_TIMESTAMP_US: timestamp_us,
        MFN.MSGDICT_SRCSYSTEM: args.srcSystem,
        MFN.MSGDICT_SRCCOMPONENT: args.srcComponent,
        MFN.MSGDICT_MAV_TYPE: mavutil.mavlink.MAV_TYPE_ONBOARD_CONTROLLER,
        MFN.MSGDICT_MAV_AUTOPILOT: mavutil.mavlink.MAV_AUTOPILOT_INVALID,
    }

    # def heartbeat_send(self, type, autopilot, base_mode, custom_mode, system_status, mavlink_version, force_mavlink1)
    mavlink_connection.mav.heartbeat_send(
        mavutil.mavlink.MAV_TYPE_ONBOARD_CONTROLLER,
        mavutil.mavlink.MAV_AUTOPILOT_INVALID,
        0, 0, 0
    )
    return datadict


def send_packet_from_msg_dict(mavlink_connection, args, msg_dict):
    # option: further enrich msg_dict for log?

    timestamp_us, time_since_boot_ms = epoch_us_and_time_since_boot_ms(args)
    msg_dict[MFN.MSGDICT_TAG_DIRECTION] = MFN.MSGDICT_TAG_DIRECTION_VALUE_SEND
    msg_dict[MFN.MSGDICT_SEND_TIMESTAMP_US] = timestamp_us
    msg_dict[MFN.MSGDICT_TIME_SINCE_BOOT_MS] = time_since_boot_ms
    msg_dict[MFN.MSGDICT_SRCSYSTEM] = args.srcSystem
    msg_dict[MFN.MSGDICT_SRCCOMPONENT] = args.srcComponent
    msg_dict[MFN.MSGDICT_TARGETSYSTEM] = args.targetSystem
    msg_dict[MFN.MSGDICT_TARGETCOMPONENT] = args.targetComponent

    if msg_dict[MFN.MSGDICT_MSG_TYPE] == MSG_TYPE_DATA_TRANSMISSION_HANDSHAKE:
        # def data_transmission_handshake_send(self, type, size, width, height, packets, payload, jpg_quality,
        #                                     force_mavlink1):
        '''
        Handshake message to initiate, control and stop image streaming when
        using the Image Transmission Protocol: 
        https://mavlink.io/en/services/image_transmission.html

        type            : Type of requested/acknowledged data. 
                            (type:uint8_t, values:MAVLINK_DATA_STREAM_TYPE)
        size            : total data size (set on ACK only). [bytes] (type:uint32_t)
        width           : Width of a matrix or image. (type:uint16_t)
        height          : Height of a matrix or image. (type:uint16_t)
        packets         : Number of packets being sent (set on ACK only). (type:uint16_t)
        payload         : Payload size per packet (normally 253 byte, 
                            see DATA field size in message ENCAPSULATED_DATA) (set on ACK only). 
                            [bytes] (type:uint8_t)
        jpg_quality     : JPEG quality. Values: [1-100]. [%] (type:uint8_t)
        '''
        result = mavlink_connection.mav.data_transmission_handshake_send(
            msg_dict[MFN.MSGDICT_MAVLINK_DATA_STREAM_TYPE],
            msg_dict[MFN.MSGDICT_BYTES_NUM],
            msg_dict[MFN.MSGDICT_IMAGE_WIDTH],
            msg_dict[MFN.MSGDICT_IMAGE_HEIGHT],
            msg_dict[MFN.MSGDICT_PACKETS_NUM],
            msg_dict[MFN.MSGDICT_PACKET_PAYLOAD_BYTES],
            msg_dict[MFN.MSGDICT_IMAGE_JPG_QUALITY]
        )
        log.debug("sent %s", MSG_TYPE_DATA_TRANSMISSION_HANDSHAKE)

    elif msg_dict[MFN.MSGDICT_MSG_TYPE] == MSG_TYPE_ENCAPSULATED_DATA:
        # def encapsulated_data_send(self, seqnr, data, force_mavlink1):
        '''
        Data packet for images sent using the Image Transmission Protocol:
        https://mavlink.io/en/services/image_transmission.html
    
        seqnr   : sequence number (starting with 0 on every transmission) (type:uint16_t)
        data    : image data bytes (type:uint8_t)
        '''

        result = mavlink_connection.mav.encapsulated_data_send(
            msg_dict[MFN.MSGDICT_MSG_SEQ],
            msg_dict[MFN.MSGDICT_DATA]
        )
        log.debug("sent %s with seq %d", MSG_TYPE_ENCAPSULATED_DATA, msg_dict[MFN.MSGDICT_MSG_SEQ])

    elif msg_dict[MFN.MSGDICT_MSG_TYPE] == MSG_TYPE_STATUSTEXT:
        # def statustext_send(self, severity, text, force_mavlink1)
        '''
                Status text message. These messages are printed in yellow in the COMM
                console of QGroundControl. WARNING: They consume quite
                some bandwidth, so use only for important status and
                error messages. If implemented wisely, these messages
                are buffered on the MCU and sent only at a limited
                rate (e.g. 10 Hz).

                severity    : Severity of status. Relies on the definitions within RFC-5424. 
                                (type:uint8_t, values:MAV_SEVERITY)
                text        : Status text message, without null termination character (type:char)
                '''
        result = mavlink_connection.mav.statustext_send(
            msg_dict[MFN.MSGDICT_STATUSTEXT_SEVERITY],
            str(msg_dict[MFN.MSGDICT_STATUSTEXT_TEXT]).encode("ascii")
        )
        log.debug("sent %s", MSG_TYPE_STATUSTEXT)

    elif msg_dict[MFN.MSGDICT_MSG_TYPE] == MSG_TYPE_NAMED_VALUE_INT:
        # def named_value_int_send(self, time_boot_ms, name, value, force_mavlink1):
        '''
                Send a key-value pair as integer. The use of this message is
                discouraged for normal packets, but a quite efficient
                way for testing new messages and getting experimental
                debug output.

                time_boot_ms              : Timestamp (time since system boot). [ms] (type:uint32_t)
                name                      : Name of the debug variable (type:char)
                value                     : Signed integer value (type:int32_t)
                '''
        # time_boot_ms value range
        # struct.error: 'I' format requires 0 <= number <= 4294967295
        # struct.pack('<Ii10s', self.time_boot_ms, self.value, self.name), force_mavlink1=force_mavlink1)

        result = mavlink_connection.mav.named_value_int_send(
            int(msg_dict[MFN.MSGDICT_TIME_SINCE_BOOT_MS]),
            str(msg_dict[MFN.MSGDICT_NAMED_VALUE_NAME]).encode("ascii"),
            msg_dict[MFN.MSGDICT_NAMED_VALUE_INT_VALUE],
        )
        log.debug("sent %", MSG_TYPE_NAMED_VALUE_INT)

    elif msg_dict[MFN.MSGDICT_MSG_TYPE] == MSG_TYPE_NAMED_VALUE_FLOAT:
        # def named_value_float_send(self, time_boot_ms, name, value, force_mavlink1)
        '''
                Send a key-value pair as float. The use of this message is discouraged
                for normal packets, but a quite efficient way for
                testing new messages and getting experimental debug
                output.

                time_boot_ms              : Timestamp (time since system boot). [ms] (type:uint32_t)
                name                      : Name of the debug variable (type:char)
                value                     : Floating point value (type:float)
                '''
        result = mavlink_connection.mav.named_value_float_send(
            int(msg_dict[MFN.MSGDICT_TIME_SINCE_BOOT_MS]),
            str(msg_dict[MFN.MSGDICT_NAMED_VALUE_NAME]).encode("ascii"),
            msg_dict[MFN.MSGDICT_NAMED_VALUE_FLOAT_VALUE],
        )
        log.debug("sent %s", MSG_TYPE_NAMED_VALUE_FLOAT)

    elif msg_dict[MFN.MSGDICT_MSG_TYPE] == MSG_TYPE_DEBUG:
        # def debug_send(self, time_boot_ms, ind, value, force_mavlink1=False):
        '''
            Send a debug value. The index is used to discriminate between values.
            These values show up in the plot of QGroundControl as
            DEBUG N.

            time_boot_ms              : Timestamp (time since system boot). [ms] (type:uint32_t)
            ind                       : index of debug variable (type:uint8_t)
            value                     : DEBUG value (type:float)
            '''
        result = mavlink_connection.mav.debug_send(
            int(msg_dict[MFN.MSGDICT_TIME_SINCE_BOOT_MS]),
            msg_dict[MFN.MSGDICT_DEBUG_VARIABLE_INDEX],
            msg_dict[MFN.MSGDICT_DEBUG_VARIABLE_VALUE]
        )
        log.debug("sent %s", MSG_TYPE_DEBUG)

    else:
        log.warn("sending %s is not implemented", msg_dict[MFN.MSGDICT_MSG_TYPE])
        result = None
        # TODO: implement

    return msg_dict, result


def attempt_recv_heartbeat_once_with_timeout(mavlink_connection,
                                             timeout=CNF.MAVLINKHELPER_RECV_TIMEOUT_HEARTBEAT_S):
    msg = mavlink_connection.recv_match(
        type=MSG_TYPE_HEARTBEAT,
        blocking=True,
        timeout=timeout
    )
    if msg:
        return msg_to_msg_dict_tagged_recv(msg)
    else:
        return None


def repeat_pingall_until_heartbeat_received(mavlink_connection, args, seq_ping_number):
    """
    Sends a ping to establish communication via udp and waits for a response
    See ping https://www.ardusub.com/developers/pymavlink.html
        part "Run pyMavlink on the companion computer"
            # Send a ping to start connection and wait for any reply.
            #  This function is necessary when using 'udpout',
            #  as described before, 'udpout' connects to 'udpin',
            #  and needs to send something to allow 'udpin' to start
            #  sending data.
    """
    msg_dict = None
    while not msg_dict:
        # ping all / all
        mavlink_connection.mav.ping_send(
            timehelper.epoch_ms(),  # Unix time in microseconds
            seq_ping_number,  # Ping number
            0,  # Request ping of all systems
            0  # Request ping of all components
            # default: force_mavlink1 = False
        )

        msg = attempt_recv_heartbeat_once_with_timeout(mavlink_connection)

        if msg:
            msg_dict = msg_to_msg_dict_tagged_recv(msg)
        else:
            time.sleep(CNF.MAVLINKHELPER_RECV_WAIT_HEARTBEAT_S)
    return msg_dict


# https://mavlink.io/en/mavgen_python/#receiving-messages
# If you just want to synchronously access the last message of a particular type
# that was received (and when it was received) you can do so using the connection's
# mavutil.messages dictionary.
# -> queue of last n received messages


# For example using the_connection set up as before, you can wait for any message as shown:
# msg = the_connection.recv_match(blocking=True)
# -> eine receive loop mit queue


# use the mavutil recv_match() method to wait for and intercept messages as they arrive:
def attempt_recv_msg_w_blocking_timeout(mavlink_connection,
                                        timeout=CNF.MAVLINKHELPER_RECV_WAIT_ANY_S,
                                        blacklist=None):
    if blacklist is None:
        blacklist = []
    msg = mavlink_connection.recv_match(
        blocking=True,
        timeout=timeout
    )
    if msg:
        if msg.get_type() in blacklist:
            return None
        else:
            return msg_to_msg_dict_tagged_recv(msg)
    return None


# use the mavutil recv_match() method to wait for and intercept messages as they arrive:
# -> sinnvoll ohne timeout?
def attempt_recv_msgdict_wo_timeout(mavlink_connection, blacklist=None):
    if blacklist is None:
        blacklist = []
    msg = mavlink_connection.recv_match(
        blocking=False,
        timeout=None
    )
    if msg:
        if msg.get_type() in blacklist:
            return None
        else:
            return msg_to_msg_dict_tagged_recv(msg)
    return None


# use the mavutil recv_match() method to wait for and intercept messages as they arrive:
# -> sinnvoll ohne timeout?
def attempt_recv_msgdicts_wo_timeout(mavlink_connection, blacklist=None):
    msg_dicts = []
    while True:
        msg_dict = attempt_recv_msgdict_wo_timeout(mavlink_connection, blacklist)
        if msg_dict is not None:
            msg_dicts.append(msg_dict)
        else:
            break
    if len(msg_dicts) > 0:
        return msg_dicts
    else:
        return None


def connect_mavlink(args):
    log.info("connecting to mavlink...")
    log.info("  srcSystem           %d", args.srcSystem)
    log.info("  srcComponent        %d", args.srcComponent)
    log.info("  srcComponentType    %d", args.srcComponentType)
    log.info("  device              %s", args.mavlinkDevice)
    log.info("  baud                %s", args.mavlinkBaud)

    mavlink_connection = mavutil.mavlink_connection(
        args.mavlinkDevice,
        baud=args.mavlinkBaud,
        source_system=args.srcSystem,
        source_component=args.srcComponent,
    )
    if mavlink_connection:
        log.info("mavlink connection OK")
    else:
        mavlink_connection = None
        log.error("mavlink connect failed")
    return mavlink_connection
