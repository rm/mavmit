#
# This file is part of mavmit <https://gitlab.com/rm/mavmit>.
# mavmit allows to transmit files via mavlink connections and to measure connection quality
#
# Copyright (C) 2021 Richard Meinsen
#
# mavmit is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# navmit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#


from argparse import ArgumentParser

from pymavlink import mavutil

from mavmit import appconfig
from mavmit import timehelper


def handle_args():
    parser = ArgumentParser(description=__doc__)

    parser.add_argument("--mavlinkDevice",
                        type=str, required=True,
                        help="mavlink communication device. e.g. 'udpin:0.0.0.0:14550' "
                             "or '/dev/ttyACM0' or 'udpout:0.0.0.0:9000'")

    parser.add_argument("--mavlinkBaud",
                        type=int, default=115200,
                        help="mavlink communication baud rate when using a serial connection")

    parser.add_argument("--srcSystem",
                        type=int, default=1,  # part of fc system
                        # type=int, default=2, # own system -> also see Type
                        help='mavlink system id of this implementation')

    parser.add_argument("--srcComponent",
                        type=int,
                        default=mavutil.mavlink.MAV_COMP_ID_GIMBAL,
                        # opt: mavutil.mavlink.MAV_COMP_ID_GIMBAL,           ~ 154 is in pypi (only one?)
                        # opt: mavutil.mavlink.MAV_COMP_ID_ONBOARD_COMPUTER, ~ 191 not in pypi
                        # opt: mavutil.mavlink.MAV_COMP_ID_USER67,           ~  91 not in pypi
                        help='mavlink component id of this implementation')

    parser.add_argument("--srcComponentType",
                        type=int, default=mavutil.mavlink.MAV_TYPE_FIXED_WING,
                        # opt: mavutil.mavlink.MAV_TYPE_FIXED_WING,         ~  1 is in pypi
                        # opt: mavutil.mavlink.MAV_TYPE_ONBOARD_CONTROLLER, ~ 18 not in pypi
                        # opt: mavutil.mavlink.MAV_TYPE_GENERIC,            ~  0 not in pypi
                        help='mavlink component type of this implementation')

    parser.add_argument("--targetSystem",
                        type=int, default=255,
                        help='mavlink system id of this implementation')

    parser.add_argument("--targetComponent",
                        type=int, default=255,
                        help='mavlink component id of this implementation, see ')

    parser.add_argument("--heartbeatInterval",
                        type=int, default=1,
                        help="interval length in seconds, defaults to 1. "
                             "no heartbeat for 0")

    parser.add_argument("--duration",
                        type=int, default=0,
                        help="duration in seconds, defaults to 0 ~ infinite")

    parser.add_argument("--logfile",
                        type=str, default=appconfig.APP_NAME + ".log",
                        help="path and name of logfile")

    parser.add_argument("--packetsfile",
                        type=str, default=None,
                        help="path and name of send and received packets file")

    parser.add_argument("--sentpacketsfile",
                        type=str,
                        help="path and name of send packets file")

    parser.add_argument("--receivedpacketsfile",
                        type=str,
                        help="path and name of received packets file")

    parser.add_argument("--inputdir",
                        type=str, required=True,
                        help="directory where the input files are expected to be written")

    parser.add_argument("--outputdir",
                        type=str, required=True,
                        help="directory where output files are written")

    parser.add_argument("--boottime_ms",
                        type=int, required=False,
                        help="milliseconds since epoch. "
                             "the value is used as baseline to calculate ms since boot.",
                        default=timehelper.epoch_ms_int())

    return parser.parse_args()
