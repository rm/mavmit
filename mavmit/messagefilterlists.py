#
# This file is part of mavmit <https://gitlab.com/rm/mavmit>.
# mavmit allows to transmit files via mavlink connections and to measure connection quality
#
# Copyright (C) 2021 Richard Meinsen
#
# mavmit is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# navmit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#


blacklist = [
    "AHRS",
    "AHRS2",
    "AHRS3",
    "HEARTBEAT",
    "HWSTATUS",
    "ATTITUDE",
    "EKF_STATUS_REPORT",
    "GLOBAL_POSITION_INT",
    "GPS_RAW_INT",
    "MEMINFO",
    "MISSION_CURRENT",
    "NAV_CONTROLLER_OUTPUT",
    "PARAM_VALUE",
    "POSITION_TARGET_GLOBAL_INT",
    "POWER_STATUS",
    "RAW_IMU",
    "RC_CHANNELS",
    "RC_CHANNELS_RAW",
    "SCALED_IMU2",
    "SCALED_PRESSURE",
    "SENSOR_OFFSETS",
    "SERVO_OUTPUT_RAW",
    "STATUSTEXT",
    "SYS_STATUS",
    "SYSTEM_TIME",
    "TIMESYNC",
    "VFR_HUD",
    "VIBRATION",
    "WIND",
]

whitelist = [
    "BAD_DATA",
    "RADIO_STATUS",
    "DATA_TRANSMISSION_HANDSHAKE"
    "ENCAPSULATED_DATA"
]
