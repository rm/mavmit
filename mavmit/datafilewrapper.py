#
# This file is part of mavmit <https://gitlab.com/rm/mavmit>.
# mavmit allows to transmit files via mavlink connections and to measure connection quality
#
# Copyright (C) 2021 Richard Meinsen
#
# mavmit is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# navmit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#


import jsonlines

from mavmit import mavmitfieldnames as MFN
from mavmit import timehelper


def args_to_header_dict(args):
    header_dict = dict()

    header_dict[MFN.HEADER_CREATETIME_US] = timehelper.epoch_us()

    if args.inputdir is not None:
        header_dict[MFN.HEADER_INPUTDIR] = args.inputdir

    if args.mavlinkDevice is not None:
        header_dict[MFN.HEADER_MAVLINKDEVICE] = args.mavlinkDevice

    if args.mavlinkBaud is not None:
        header_dict[MFN.HEADER_MAVLINKBAUD] = args.mavlinkBaud

    if args.srcSystem is not None:
        header_dict[MFN.HEADER_SRCSYSTEM] = args.srcSystem

    if args.srcComponent is not None:
        header_dict[MFN.HEADER_SRCOMPONENT] = args.srcComponent

    if args.srcComponentType is not None:
        header_dict[MFN.HEADER_SRCOMPONENTTYPE] = args.srcComponentType

    if args.targetSystem is not None:
        header_dict[MFN.HEADER_TARGETSYSTEM] = args.targetSystem

    if args.targetComponent is not None:
        header_dict[MFN.HEADER_TARGETCOMPONENT] = args.targetComponent

    if args.heartbeatInterval is not None:
        header_dict[MFN.HEADER_HEARTBEATINTERVAL] = args.heartbeatInterval

    if args.duration is not None:
        header_dict[MFN.HEADER_DURATION] = args.duration

    if args.logfile is not None:
        header_dict[MFN.HEADER_LOGFILE] = args.logfile

    if args.packetsfile is not None:
        header_dict[MFN.HEADER_PACKETSFILE] = args.packetsfile

    if args.sentpacketsfile is not None:
        header_dict[MFN.HEADER_SENTPACKETSFILE] = args.sentpacketsfile

    if args.receivedpacketsfile is not None:
        header_dict[MFN.HEADER_RECEIVEDPACKETSFILE] = args.receivedpacketsfile

    if args.boottime_ms is not None:
        header_dict[MFN.HEADER_BOOTTIME_MS] = args.boottime_ms

    return header_dict


class DataFileWrapper:

    def __init__(self, filename, header_dict):
        nested_header_dict = dict()
        nested_header_dict[MFN.HEADER_ITEM] = header_dict
        self.__filename = filename
        self.__file = open(filename, "w")
        self.__writer = jsonlines.Writer(self.__file)
        self.__writer.write(header_dict)
        # self.__file.flush()

    def write_item(self, item):
        self.__writer.write(item)
        # self.__file.flush()

    def finish(self):
        # self.__file.flush()
        self.__writer.close()
        self.__file.close()
