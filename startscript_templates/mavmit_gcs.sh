#!/bin/bash

now=`date +"%Y-%m-%d_%H-%M-%s"`

echo "start mavmit on gcs @ time ${now}"

python3 -m mavmit \
  --mavlinkDevice=/dev/ttyUSB0 \
  --mavlinkBaud=57600 \
  --srcSystem=255 \
  --srcComponent=255 \
  --srcComponentType=1 \
  --targetSystem=1  \
  --targetComponent=1 \
  --heartbeatInterval=1 \
  --logfile=logs_r/transmit-${now}.log \
  --packetsfile=packets_r/transmit-${now}.allpackets.jl \
  --sentpacketsfile=packets_r/transmit-${now}.sentpackets.jl \
  --receivedpacketsfile=packets_r/transmit-${now}.receivedpackets.jl \
  --inputdir=./inbox_gcs \
  --outputdir=./outbox_gcs

