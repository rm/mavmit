#!/bin/bash

now=`date +"%Y-%m-%d_%H-%M-%s"`

echo "start mavmit on uav @ time ${now}"

python3 -m mavmit \
  --mavlinkDevice=/dev/ttyUSB0 \
  --mavlinkBaud=57600 \
  --srcSystem=255 \
  --srcComponent=255 \
  --srcComponentType=1 \
  --targetSystem=1  \
  --targetComponent=1 \
  --heartbeatInterval=1 \
  --logfile=logs_s/transmit-${now}.log \
  --packetsfile=packets_s/transmit-${now}.allpackets.jl \
  --sentpacketsfile=packets_s/transmit-${now}.sentpackets.jl \
  --receivedpacketsfile=packets_s/transmit-${now}.receivedpackets.jl \
  --inputdir=./inbox_uav \
  --outputdir=./outbox_uav \
