import pathlib

from setuptools import setup

HERE = pathlib.Path(__file__).parent
README = (HERE / "README.md").read_text()

setup(
    name="mavmit",
    version="0.0.1",
    description="mavmit allows to transmit files via mavlink connections "
                "and to measure connection quality",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/rm/mavmit",
    author="Richard Meinsen",
    author_email="richard.meinsen+oss@gmail.com",
    license="LGPL v3",
    keywords="mavlink, images, statustext, namedvalue, transmission, logging",
    classifiers=[
        'OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
        'Natural Language :: English',
        'Intended Audience :: Developers',
        'Development Status :: 4 - Beta',
        'Topic :: Utilities',
        'Environment :: Console',
        'Framework :: pymavlink'
    ],
    packages=["mavmit"],
    include_package_data=True,
    install_requires=[
        "pymavlink",
        "jsonlines",
        "watchdog",
        "pillow"],
    entry_points={
        "console_scripts": [
            "realpython=mavmit.__main__:main",
        ]
    },
)
