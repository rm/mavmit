#!/bin/bash

now=`date +"%Y-%m-%d_%H-%M-%s"`

echo "start file send @ time ${now}"

python3 -m mavmit \
  --mavlinkDevice=/dev/ttyUSB0 \
  --mavlinkBaud=57600 \
  --srcSystem=123 \
  --srcComponent=123 \
  --srcComponentType=1 \
  --targetSystem=234 \
  --targetComponent=234 \
  --heartbeatInterval=1 \
  --logfile=logs_r/transmit-${now}.log \
  --packetsfile=packets_r/transmit-${now}.allpackets.jl \
  --sentpacketsfile=packets_r/transmit-${now}.sentpackets.jl \
  --receivedpacketsfile=packets_r/transmit-${now}.receivedpackets.jl \
  --inputdir=./inbox_r \
  --outputdir=./outbox_r \
  --duration=2000
