#!/bin/bash

cp samplefiles_rand/a.statustext inbox_s

cp samplefiles_rand/1.statustext inbox_s
cp samplefiles_rand/1.jpg inbox_s
cp samplefiles_rand/1.namedvalueint inbox_s
cp samplefiles_rand/1.png inbox_s
cp samplefiles_rand/1.namedvaluefloat inbox_s
cp samplefiles_rand/1.bmp inbox_s

cp samplefiles_rand/2.namedvaluefloat inbox_s
cp samplefiles_rand/2.bmp inbox_s
cp samplefiles_rand/2.namedvalueint inbox_s
cp samplefiles_rand/2.png inbox_s
cp samplefiles_rand/2.statustext inbox_s
cp samplefiles_rand/2.jpg inbox_s

sleep 20

cp samplefiles_rand/o.statustext inbox_s
