#!/bin/bash

cp samplefiles_4d6e8f/a.statustext                    inbox_s

cp samplefiles_4d6e8f/1.statustext                    inbox_s
cp samplefiles_4d6e8f/4d6e8f_777_w_colourspace.bmp    inbox_s
cp samplefiles_4d6e8f/1.namedvalueint                 inbox_s
cp samplefiles_4d6e8f/4d6e8f_cmpr0.png                inbox_s
cp samplefiles_4d6e8f/1.namedvaluefloat               inbox_s
cp samplefiles_4d6e8f/4d6e8f_q00.jpg                  inbox_s

cp samplefiles_4d6e8f/2.namedvaluefloat               inbox_s
cp samplefiles_4d6e8f/4d6e8f_cmpr9.png                inbox_s
cp samplefiles_4d6e8f/2.namedvalueint                 inbox_s
cp samplefiles_4d6e8f/4d6e8f_q90.jpg                  inbox_s
cp samplefiles_4d6e8f/2.statustext                    inbox_s
cp samplefiles_4d6e8f/4d6e8f_777_wocolourspace.bmp    inbox_s

sleep 20

cp samplefiles_4d6e8f/o.statustext                    inbox_s
