#!/bin/bash

now=`date +"%Y-%m-%d_%H-%M-%s"`

echo "start file send @ time ${now}"

python3 -m mavmit \
  --mavlinkDevice=/dev/ttyUSB0 \
  --mavlinkBaud=57600 \
  --srcSystem=234 \
  --srcComponent=234 \
  --srcComponentType=1 \
  --targetSystem=123 \
  --targetComponent=123 \
  --heartbeatInterval=1 \
  --logfile=logs_s/transmit-${now}.log \
  --packetsfile=packets_s/transmit-${now}.allpackets.jl \
  --sentpacketsfile=packets_s/transmit-${now}.sentpackets.jl \
  --receivedpacketsfile=packets_s/transmit-${now}.receivedpackets.jl \
  --inputdir=./inbox_s \
  --outputdir=./outbox_s \
  --duration=2000
