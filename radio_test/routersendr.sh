#!/bin/bash

now=`date +"%Y-%m-%d_%H-%M-%s"`

echo "start file send @ time ${now}"
echo "have mavlink-routerd -e 0.0.0.0:4000 -e 0.0.0.0:3000 running"

python3 -m mavmit \
  --mavlinkDevice=tcp:0.0.0.0:5760 \
  --srcSystem=234 \
  --srcComponent=234 \
  --srcComponentType=1  \
  --targetSystem=123 \
  --targetComponent=123 \
  --heartbeatInterval=1 \
  --logfile=logs_s/transmit-${now}.log \
  --packetsfile=packets_s/transmit-${now}.allpackets.jl \
  --sentpacketsfile=packets_s/transmit-${now}.sentpackets.jl \
  --receivedpacketsfile=packets_s/transmit-${now}.receivedpackets.jl \
  --inputdir=./inbox_s \
  --outputdir=./outbox_s \
  --duration=2000
