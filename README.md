# TL;DR

`mavmit` allows transmitting files via mavlink connections and to analyse connection quality.

`mavmit`is published under LGPLv3

# Details

## Motivation

`mavmit` was developed for two main reasons
- transmitting small images from an uav companion to the gcs 
  - without having the gcs to ask for transmission
  - without having to use more complex mavlink protocols like
    - ftp
    - image streaming
- analyzing the mavlink connection
  - to find error causes like defect hardware
  - to tune the connection parameters for (more) reliable transmission
    
## Usage

`mavmit` is based on <https://github.com/ArduPilot/pymavlink>.
`pymavlink` can be installed and configured for different dialects. 
For many use cases including `mavmit` it's perfectly fine to install 
`pymavlink` from https://pypi.org/project/pymavlink/ but be aware 
that depending on what you want to achieve installation from source 
can be the better choice.
        
~~~(text)
$ python -m mavmit -h
usage: __main__.py [-h] --mavlinkDevice MAVLINKDEVICE [--mavlinkBaud MAVLINKBAUD] [--srcSystem SRCSYSTEM]
                   [--srcComponent SRCCOMPONENT] [--srcComponentType SRCCOMPONENTTYPE] [--targetSystem TARGETSYSTEM]
                   [--targetComponent TARGETCOMPONENT] [--heartbeatInterval HEARTBEATINTERVAL] [--duration DURATION]
                   [--logfile LOGFILE] [--packetsfile PACKETSFILE] [--sentpacketsfile SENTPACKETSFILE]
                   [--receivedpacketsfile RECEIVEDPACKETSFILE] --inputdir INPUTDIR --outputdir OUTPUTDIR
                   [--boottime_ms BOOTTIME_MS]

optional arguments:
  -h, --help            show this help message and exit
  --mavlinkDevice MAVLINKDEVICE
                        mavlink communication device. e.g. 'udpin:0.0.0.0:14550' or '/dev/ttyACM0' or 'udpout:0.0.0.0:9000'
  --mavlinkBaud MAVLINKBAUD
                        mavlink communication baud rate when using a serial connection
  --srcSystem SRCSYSTEM
                        mavlink system id of this implementation
  --srcComponent SRCCOMPONENT
                        mavlink component id of this implementation
  --srcComponentType SRCCOMPONENTTYPE
                        mavlink component type of this implementation
  --targetSystem TARGETSYSTEM
                        mavlink system id of this implementation
  --targetComponent TARGETCOMPONENT
                        mavlink component id of this implementation, see
  --heartbeatInterval HEARTBEATINTERVAL
                        interval length in seconds, defaults to 1. no heartbeat for 0
  --duration DURATION   duration in seconds, defaults to 0 ~ infinite
  --logfile LOGFILE     path and name of logfile
  --packetsfile PACKETSFILE
                        path and name of send and received packets file
  --sentpacketsfile SENTPACKETSFILE
                        path and name of send packets file
  --receivedpacketsfile RECEIVEDPACKETSFILE
                        path and name of received packets file
  --inputdir INPUTDIR   directory where the input files are expected to be written
  --outputdir OUTPUTDIR
                        directory where output files are written
  --boottime_ms BOOTTIME_MS
                        milliseconds since epoch. the value is used as baseline to calculate ms since boot.
~~~

A running `mavmit` instance expects specific files in a specified directory 
and transmits the files contents to another running `mavmit` instance.

A running `mavmit` instance expects files via mavlink connection and 
writes the files contents as files to a specified directory. 
The received files are named by timestamp.


The specific files are:
- image files with suffix `.jpg` or `.jpeg`, `.png`, `.bmp` 
- raw data files with suffix `.raw32u` or `.raw8u` 
- statustext files with suffix `.statustext`  
- named value float files with suffix `.namedvaluefloat`  
- named value int files with suffix `.namedvalueint`  


A statustext files looks like:
~~~(text)
a statustext has severity:7
~~~
See <https://mavlink.io/en/messages/common.html#STATUSTEXT> 
for further info. ´id´ and ´chunk_seq´ are not supported for now,


A named value float file looks like:
~~~(text)
avaluename:1.1
~~~
See <https://mavlink.io/en/messages/common.html#NAMED_VALUE_FLOAT>
for further info. The length of the name is 10 bytes and it will be
truncated if longer. The name is handled as ascii encoded. 


A named value int file looks like:
~~~(text)
bvaluename:1
~~~
See <https://mavlink.io/en/messages/common.html#NAMED_VALUE_INT>
for further info. The length of the name is 10 bytes and it will be
truncated if longer. The name is handled as ascii encoded. 


The sent and received mavlink packets can be logged in JSON lines format, 
see <https://jsonlines.org/>, to files in a specified directory.


Additionally, a less detailed logfile can be written.

## Technical background

The mavlink protocol is designed to be lightweight for usage in FCs 
in combination with low bandwidth and also possibly instable radio 
connections.

The mavlink protocol aims to make best use of limited bandwidth. For the 
implementation the perfect adherence to the written protocol is less 
important than getting things done even with lost mavlink packets.

Mavlink routing, as far as I know it, doesn't do much checking if the
written protocol specs are obeyed, it just forwards packets.

The Image Transmission Protocol 
<https://mavlink.io/en/services/image_transmission.html> 
specifies that the GCS initiates the image transmission by a 
DATA_TRANSMISSION_HANDSHAKE that is acknowledged from the UAV by a 
DATA_TRANSMISSION_HANDSHAKE followed by (image) data in ENCAPSULATED_DATA 
packets.

The `mavmit` implementation doesn't send the initiating 
DATA_TRANSMISSION_HANDSHAKE  (from the GCS). The transmission is 
started from the UAV  by sending DATA_TRANSMISSION_HANDSHAKE followed 
by (image) data in ENCAPSULATED_DATA packets.

In mavlink routing (as far as I worked with it) there is no 
- state checking done, like: has a DATA_TRANSMISSION_HANDSHAKE been sent 
  from the gcs side first
- content checking done, like: do the image packets have the right content 
  (would be hard for RAW32U or RAW8U)


## Analysis

I started working with mavlink because I wanted to help searchwing 
https://www.hs-augsburg.de/searchwing/de/willkommen/ with the image 
transmission use case.

To check my code I used two 433MHz radios. After wondering why the 
first simple data transmissions failed I also looked at transmission 
errors. It turned out that the radios caused much of the problems.

To get a clearer picture about transmission error rate in different 
conditions 'mavmit' allows extensive logging/data capture. 

Further there is a jupyter notebook in the radio_analysis folder 
that can be used to analyse the captured data and visualise 
transmission errors. Some small data sets are also contained.

## Development 

For your convenience find two start param lists for pycharm, ... for development.

~~~(text)
--mavlinkDevice=/dev/ttyUSB0  --mavlinkBaud=57600  --srcSystem=123  --srcComponent=123  --srcComponentType=1  --targetSystem=234  --targetComponent=234  --heartbeatInterval=1  --logfile=logs_r/transmit-____.log  --packetsfile=packets_r/transmit-____.allpackets.jl  --sentpacketsfile=packets_r/transmit-____.sentpackets.jl  --receivedpacketsfile=packets_r/transmit-____.receivedpackets.jl  --inputdir=./inbox_r  --outputdir=./outbox_r  --duration=2000
~~~

~~~(text)
--mavlinkDevice=/dev/ttyUSB0  --mavlinkBaud=57600  --srcSystem=234  --srcComponent=234  --srcComponentType=1  --targetSystem=1  --targetComponent=123  --heartbeatInterval=123  --logfile=logs_r/transmit-____.log  --packetsfile=packets_r/transmit-____.allpackets.jl  --sentpacketsfile=packets_s/transmit-____.sentpackets.jl  --receivedpacketsfile=packets_s/transmit-____.receivedpackets.jl  --inputdir=./inbox_s  --outputdir=./outbox_s  --duration=2000
~~~
